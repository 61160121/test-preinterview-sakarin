package main

import (
	"net/http"
)

var (
	userMux *http.ServeMux
)

func init() {
	userMux = NewMux()
}

func main() {
	http.HandleFunc("/", User)
	http.ListenAndServe(":8080", nil)
}

func User(w http.ResponseWriter, r *http.Request) {

	userMux.ServeHTTP(w, r)

}

//
func NewMux() *http.ServeMux {
	mux := http.NewServeMux()

	mux.HandleFunc("/test1", Test1)
	mux.HandleFunc("/test4", Test4)
	mux.HandleFunc("/test5", Test5)

	return mux

}
