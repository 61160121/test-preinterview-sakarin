package main

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

//ข้อ1
func Test1(w http.ResponseWriter, r *http.Request) {

	//ข้อ1
	// var (
	// 	a = 1200
	// 	b = 95
	// 	c = 115
	// )

	switch r.Method {
	case http.MethodPost:
	default:
		ErrorResponse(
			w,
			http.StatusForbidden,
			"This function use method POST.",
		)
		return
	}

	var testnuber1 TestNuber1

	var result float64
	var result1 float64
	var result2 float64

	r.ParseForm()

	r.ParseMultipartForm(10 << 20)

	a := strings.TrimSpace(r.PostForm.Get("a"))
	b := strings.TrimSpace(r.PostForm.Get("b"))
	c := strings.TrimSpace(r.PostForm.Get("c"))

	resultA, _ := strconv.Atoi(a)
	resultB, _ := strconv.Atoi(b)
	resultC, _ := strconv.Atoi(c)

	result, result1, result2 = Validate1(float64(resultA), float64(resultB), float64(resultC))

	testnuber1.Result = fmt.Sprintf("%.2f", result)
	testnuber1.Result1 = fmt.Sprintf("%.2f", result1)
	testnuber1.Result2 = fmt.Sprintf("%.2f", result2)

	fmt.Println("Percent Year type a is", testnuber1.Result, "%")
	fmt.Println("Percent Year type b is", testnuber1.Result1, "%")
	fmt.Println("Percent Year type c is", testnuber1.Result2, "%")
	//ข้อ1

	data := map[string]interface{}{
		"data":    testnuber1,
		"message": "Success!",
	}

	SuccessResponse(w, http.StatusOK, data)
}

//ข้อ1

//ข้อ4
func Test4(w http.ResponseWriter, r *http.Request) {

	var testnuber4 TestNuber4

	switch r.Method {
	case http.MethodPost:
	default:
		ErrorResponse(
			w,
			http.StatusForbidden,
			"This function use method POST.",
		)
		return
	}

	r.ParseForm()

	r.ParseMultipartForm(10 << 20)

	symbol := strings.TrimSpace(r.PostForm.Get("symbol"))

	//ข้อ4
	testnuber4.Result = Validate4(symbol)
	//"[[[{{}}]]]"
	//ข้อ4

	fmt.Println("Result : ", testnuber4.Result)

	data := map[string]interface{}{
		"data":    testnuber4,
		"message": "Success!",
	}

	SuccessResponse(w, http.StatusOK, data)
}

//ข้อ4

//ข้อ5
func Test5(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case http.MethodPost:
	default:
		ErrorResponse(
			w,
			http.StatusForbidden,
			"This function use method POST.",
		)
		return
	}

	var test5 TestNuber

	r.ParseForm()

	r.ParseMultipartForm(10 << 20)

	n := strings.TrimSpace(r.PostForm.Get("n"))
	m := strings.TrimSpace(r.PostForm.Get("m"))

	resultN, _ := strconv.Atoi(n)
	resultM, _ := strconv.Atoi(m)

	test5.Result = Validate5(resultN, resultM)

	fmt.Println("Summary of number lenght : ", test5.Result)

	data := map[string]interface{}{
		"data":    test5,
		"message": "Success!",
	}

	SuccessResponse(w, http.StatusOK, data)
}

//ข้อ5
